﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace WindowManagerThingy
{
    [Serializable]
    public struct WindowDetails {
        public user32.WINDOWPLACEMENT wndp;
        public string wndname;
        public override String ToString()
        {
            return this.wndname;
        }
    }

    class window
    {
        public static WindowDetails getWindowDetails(string s) {
            WindowDetails wndd;

            wndd.wndname = s;
            wndd.wndp = user32.getWndPl(s);

            return wndd;
        }

        public static int getState(WindowDetails wndd)
        {
            return wndd.wndp.showCmd;
        }

        public static void setState(ref WindowDetails wndd, int i)
        {
            wndd.wndp.showCmd = i;
        }

        public static void saveFile(string fn, List<WindowDetails> wd) {
            /** Grabs the List object and turns it into a binary file.
             *  Not pretty, but it does the job.
             * 
             */

            BinaryFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fn, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, wd);
            stream.Close();
        }

        public static List<WindowDetails> loadFile(string fn) {  
            /** Loads the binary file and returns a List object with its contents.
             * 
             */ 

            BinaryFormatter bf = new BinaryFormatter();
            Stream readStream = new FileStream(fn, FileMode.Open, FileAccess.Read, FileShare.Read);
            List<WindowDetails> wd = (List<WindowDetails>)bf.Deserialize(readStream);
            readStream.Close();
            return wd;
        }
    }
}
/*  Copyright (c) 2012 Samuel Rodrigues Pedro
    See the file LICENSE for copying permission.
    */
