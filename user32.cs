﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Text.RegularExpressions;

namespace WindowManagerThingy
{
    public class user32
    {
        public delegate bool EnumDelegate(IntPtr hWnd, int lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hWnd);


        [DllImport("user32.dll", EntryPoint = "GetWindowText",
        ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpWindowText, int nMaxCount);


        [DllImport("user32.dll", EntryPoint = "EnumDesktopWindows",
        ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool EnumDesktopWindows(IntPtr hDesktop, EnumDelegate lpEnumCallbackFunction, IntPtr lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindow(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);
        

         public enum SystemMetric{
            SM_CXSCREEN         = 0,
            SM_CYSCREEN         = 1
         }

         [DllImport("user32.dll")]
         static extern int GetSystemMetrics(SystemMetric smIndex);
        private enum SetWindowPosFlags : uint
        {
            
            SynchronousWindowPosition = 0x4000,
            DeferErase = 0x2000,
            DrawFrame = 0x0020,
            FrameChanged = 0x0020,
            HideWindow = 0x0080,
            DoNotActivate = 0x0010,
            DoNotCopyBits = 0x0100,
            IgnoreMove = 0x0002,
            DoNotChangeOwnerZOrder = 0x0200,
            DoNotRedraw = 0x0008,
            DoNotReposition = 0x0200,
            DoNotSendChangingEvent = 0x0400,
            IgnoreResize = 0x0001,
            IgnoreZOrder = 0x0004,
            ShowWindow = 0x0040,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;        
            public int Top;        
            public int Right;      
            public int Bottom;    
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [Serializable]
        public struct WINDOWPLACEMENT {
                public int length;
                public int flags;
                public int showCmd;
                public System.Drawing.Point ptMinPosition;
                public System.Drawing.Point ptMaxPosition;
                public System.Drawing.Rectangle rcNormalPosition;
        }

        public static List<string> getList()
        {
            var collection = new List<string>();
            user32.EnumDelegate filter = delegate(IntPtr hWnd, int lParam)
            {
                StringBuilder strbTitle = new StringBuilder(1023);
                int nLength = user32.GetWindowText(hWnd, strbTitle, strbTitle.Capacity + 1);
                string strTitle = strbTitle.ToString();

                if (user32.IsWindowVisible(hWnd) && string.IsNullOrEmpty(strTitle) == false)
                {
                    collection.Add(strTitle);
                }
                return true;
            };

            user32.EnumDesktopWindows(IntPtr.Zero, filter, IntPtr.Zero);
            return collection;
        }

        public static Rectangle getRect(string s) {
            RECT rct;
            Rectangle rect = new Rectangle();

            IntPtr hwnd = FindWindow(IntPtr.Zero, s);

            GetWindowRect(hwnd, out rct);

            rect.X = rct.Left;
            rect.Y = rct.Top;
            rect.Width = rct.Right - rct.Left + 1;
            rect.Height = rct.Bottom - rct.Top + 1;

            return rect;
        }

        public static WINDOWPLACEMENT getWndPl(string s)
        {
            IntPtr hwnd = FindWindow(IntPtr.Zero, s);
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            if (hwnd != IntPtr.Zero)
            {
                GetWindowPlacement(hwnd, ref placement);           
            }
            return placement;
        }


        public static List<WindowDetails> getListDetails()
        {
            var collection = new List<WindowDetails>();
            user32.EnumDelegate filter = delegate(IntPtr hWnd, int lParam)
            {
                WindowDetails wndd;
                WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                if (hWnd != IntPtr.Zero)
                {
                    GetWindowPlacement(hWnd, ref placement);
                }

                StringBuilder strbTitle = new StringBuilder(1023);
                int nLength = user32.GetWindowText(hWnd, strbTitle, strbTitle.Capacity + 1);
                string strTitle = strbTitle.ToString();

                if (user32.IsWindowVisible(hWnd) && string.IsNullOrEmpty(strTitle) == false)
                {
                    wndd.wndp = placement;
                    wndd.wndname = strTitle;
                    collection.Add(wndd);
                }
                return true;
            };

            user32.EnumDesktopWindows(IntPtr.Zero, filter, IntPtr.Zero);
            return collection;
        }

        public static void setWindow(WindowDetails wd)
        {
            IntPtr wndh;

            user32.EnumDelegate filter = delegate(IntPtr hWnd, int lParam)
            {
                StringBuilder strbTitle = new StringBuilder(1023);
                int nLength = user32.GetWindowText(hWnd, strbTitle, strbTitle.Capacity + 1);
                string strTitle = strbTitle.ToString();

                if (user32.IsWindowVisible(hWnd) && string.IsNullOrEmpty(strTitle) == false)
                {
                    if (Regex.IsMatch(strTitle, wd.wndname))
                    {
                        wndh = hWnd;
                        SetWindowPlacement(hWnd, ref wd.wndp);

                    }
                }
                return true;
            };

            user32.EnumDesktopWindows(IntPtr.Zero, filter, IntPtr.Zero);
        }
    }
}

/*  Copyright (c) 2012 Samuel Rodrigues Pedro
    See the file LICENSE for copying permission.
    */
