﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WindowManagerThingy
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (lckBt.Checked)
            {
                wndNm.Enabled = false;
                posX.Enabled = false;
                posY.Enabled = false;
                wdt.Enabled = false;
                hgt.Enabled = false;
                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton3.Enabled = false;
            }
            else
            {
                wndNm.Enabled = true;
                posX.Enabled = true;
                posY.Enabled = true;
                wdt.Enabled = true;
                hgt.Enabled = true;
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if(listBox2.Items.Count > 0){
                user32.getListDetails().ForEach(delegate(WindowDetails wd){            
                    foreach(WindowDetails i in listBox2.Items){
                        if(!(Regex.IsMatch(wd.ToString(), i.ToString()))){
                            listBox1.Items.Add(wd);
                        }
                    }
                });
            }else{
                user32.getListDetails().ForEach(delegate(WindowDetails wd)
                {
                   listBox1.Items.Add(wd);
                });
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.SelectedIndex = listBox2.Items.Add(listBox1.SelectedItem);
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox1.Items.Add(listBox2.SelectedItem);
            listBox2.Items.Remove(listBox2.SelectedItem);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button2.Enabled = true;
            }
            else {
                button2.Enabled = false;
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                panel2.Visible = false;
                button3.Enabled = true;
                wndNm.Text = listBox2.SelectedItem.ToString();

                switch (window.getState((WindowDetails)listBox2.SelectedItem))
                { 
                    case 1: // Normal
                        radioButton2.Checked = true;
                        break;
                    case 2: // Minimized
                        radioButton1.Checked = true;
                        break;
                    case 3: // Maximized
                        radioButton3.Checked = true;
                        break;
                }
                    
                    panel2.Visible = false;
                    WindowDetails wd = (WindowDetails)listBox2.SelectedItem;
                    panel1.Visible = false;
                    posX.Text = wd.wndp.rcNormalPosition.X.ToString();
                    posY.Text = wd.wndp.rcNormalPosition.Y.ToString();
                    wdt.Text = (wd.wndp.rcNormalPosition.Width - wd.wndp.rcNormalPosition.X).ToString();
                    hgt.Text = (wd.wndp.rcNormalPosition.Height - wd.wndp.rcNormalPosition.Y).ToString();
                    lckBt.Checked = true;
                //}
            }
            else
            {
                button3.Enabled = false;
                panel2.Visible = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            button3.Enabled = false;
            panel2.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            WindowDetails wd = (WindowDetails)listBox2.SelectedItem;



            if (radioButton2.Checked) { 
                // Normal
                window.setState(ref wd, 1);
            }else if (radioButton1.Checked) {
                // Minimized
                window.setState(ref wd, 2);
            }else if (radioButton3.Checked) {
                // Maximized
                window.setState(ref wd, 3);
            }

            wd.wndname = wndNm.Text;
            wd.wndp.rcNormalPosition.X = Convert.ToInt32(posX.Text);
            wd.wndp.rcNormalPosition.Y = Convert.ToInt32(posY.Text);
            wd.wndp.rcNormalPosition.Width = Convert.ToInt32(wdt.Text) + Convert.ToInt32(posX.Text);
            wd.wndp.rcNormalPosition.Height = Convert.ToInt32(hgt.Text) + Convert.ToInt32(posY.Text);

            listBox2.Items.Remove(listBox2.SelectedItem);
            listBox2.SelectedIndex = listBox2.Items.Add(wd);
            listBox2.Refresh();
            lckBt.Checked = true;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach(WindowDetails wd in listBox2.Items) {
                user32.setWindow(wd); 
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            WindowDetails wd = (WindowDetails)listBox2.SelectedItem;

            wd = window.getWindowDetails(wd.wndname);

                
            listBox2.Items.Remove(listBox2.SelectedItem);
            listBox2.SelectedIndex = listBox2.Items.Add(wd);
            listBox2.Refresh();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            WindowDetails wd = (WindowDetails)listBox2.SelectedItem;
            user32.setWindow(wd);
        }

        private void loadFile(String file)
        {
            listBox2.Items.Clear();
            try
            {
                using (Stream stream = File.Open(file, FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    var wdl = (List<WindowDetails>)bin.Deserialize(stream);
                    foreach (WindowDetails wd in wdl)
                    {

                        listBox2.SelectedIndex = listBox2.Items.Add(wd);
                        listBox2.Refresh();
                    }
                }
            }
            catch (IOException)
            {
            }
            listBox1.Items.Clear();
            if (listBox2.Items.Count > 0)
            {
                user32.getListDetails().ForEach(delegate(WindowDetails wd)
                {
                    foreach (WindowDetails i in listBox2.Items)
                    {
                        if (!(Regex.IsMatch(wd.ToString(), i.ToString())))
                        {
                            listBox1.Items.Add(wd);
                        }
                    }
                });
            }
            else
            {
                user32.getListDetails().ForEach(delegate(WindowDetails wd)
                {
                    listBox1.Items.Add(wd);
                });
            }
        }

        private void saveFile(String file)
        {
            List<WindowDetails> wd = listBox2.Items.Cast<WindowDetails>().ToList();
            try
            {
                using (Stream stream = File.Open(file, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, wd);
                }
            }
            catch (IOException)
            {
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog(); 
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments); 
            saveFileDialog1.Filter = "WindowManagerThingySettings (*.wts)|*.wts|All Files (*.*)|*.*" ; 
            saveFileDialog1.FilterIndex = 1;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                saveFile(saveFileDialog1.FileName);
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            openFileDialog1.Filter = "WindowManagerThingySettings (*.wts)|*.wts|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                loadFile(openFileDialog1.FileName);
            }

        }

        private void button12_Click(object sender, EventArgs e)
        {
            panel3.Hide();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (panel3.Visible == false)
            {
                panel3.Show();
            }
            else
            {
                panel3.Hide();
            }
        }

    }
}

/*  Copyright (c) 2012 Samuel Rodrigues Pedro
    See the file LICENSE for copying permission.
    */
