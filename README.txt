    ##  Window Manager Thingy Readme  ##
    
-------------------------------------------------------------------------------

 # Introduction

    Window Manager Thingy(WiMaTy) is a small aplication for managing window 
  positions on a Windows OS.
    It was developed on Windows 7 with Visual C# 2010 Express with the purpose
  of saving/changing the position of windows so you can save your layouts and 
  and switch between them.
  
  
 # Description
    
    I started working on this application because some changing resolutions on
  my dual-monitor setup kept messing the position of my media player and other
  applications on my second monitor, and existing applications would only allow
  the repositioning of one window at a time.
    This was pretty much a learning experience because I had no idea how to use
  something like this window API.
    After a few mornings of Googling and throwing code at Visual Studio until
  things worked I had a usable application that fitted my needs.
    Since then I kept adding features as needed (the latest one being 
  saving/loading of layouts) and am now getting this online in case someone can
  make use of it.
    
  
 # Usage

    After opening the application, clicking the Refresh button will list open
  windows which can then be added to the positioning list on the right side of 
  the application by selecting the window name and pressing the ">" button.
    Selecting the window's name will allow you to change it's position by 
  editing it's values.
    Pressing "Position" will set the window's position to it's saved values,
  and using the "Position ALL THE THINGS!" will position all of the windows on
  the positioning list.
    Saving/loading lists can be used to have several list profiles and changing
  window names on the details to RegEx is possible to allow repositioning of 
  applications with variable names such as browsers/media players/file editors,
  etc.

  
 # Build Instructions
  
    Just open the .csproj file with Visual C# and you should be able to build
  it without any problems.
  
  
 # Disclaimer
 
    This application was made by someone who was learning while developing it,
  which means the code is going to be dirty, messy and ugly.
    I do not guarantee it will work on your OS (was only tested on Win7 x64).
    If it explodes your computer, poops on your lawn, turns you into a newt and
   even if, worst of all, it plain doesn't work, I take no responsibility but I
   will try to help you if I can. =D
   
   
 # Licensing
 
    See the file LICENSE for copying permission.

  
 # Credits
 
    www.pinvoke.net   - Essential for me to learn how to use Window API.
    stackoverflow.com - Awesome source for solving small coding problems.
    
    
 # Author
 
    Samuel Rodrigues Pedro
    E-mail: sam.r.pedro@gmail.com
    Twitter: @Xenopeg
    
-------------------------------------------------------------------------------